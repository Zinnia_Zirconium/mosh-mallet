#!/bin/bash
if [[ "${1}" =~ ^[^-] ]] && [[ "${2}" =~ ^[^-] ]] ; then
if [[ ${#MOSH_MALLET_HMAC_KEY} -eq 0 ||
${#MOSH_MALLET_CCRYPT_KEY} -eq 0 ]] ; then
prompting=1 ; else prompting=0 ; fi
if [[ "${prompting}" -eq 1 ]] ; then
read -r -s -p "Enter HMAC key: " ; echo >&2
MOSH_MALLET_HMAC_KEY="${REPLY}"
export MOSH_MALLET_HMAC_KEY ; fi
request="$( php -r '
$stamp = time();
$key = getenv("MOSH_MALLET_HMAC_KEY");
if (false !== $key) {
$hash = hash_hmac("sha256", $stamp, $key, true);
if (false !== $hash) {
echo base64_encode($hash . $stamp); } } ' )"
if [[ ${#request} -ne 0 ]] ; then
response="$( echo "${request}" | \
{ base64 -d ; sleep 5 ; } | \
nc -u -w 5 "${1}" "${2}" | base64 )"
if [[ ${#response} -ne 0 ]] ; then
content="$( echo "${response}" | \
php -r '
$base64_response = stream_get_contents(STDIN);
if (false !== $base64_response) {
if (strlen($base64_response)) {
$response = base64_decode($base64_response);
if (false !== $response) {
if (32 < strlen($response)) {
$response_hash = substr($response, 0, 32);
$response_mosh = substr($response, 32);
$key = getenv("MOSH_MALLET_HMAC_KEY");
if (false !== $key) {
$hash = hash_hmac("sha256", $response_mosh, $key, true);
if (false !== $hash) {
if (hash_equals($hash, $response_hash)) {
echo base64_encode($response_mosh); }
else fwrite(STDERR, "integrity check failed\n");
} } } } } }' )"
if [[ ${#content} -ne 0 ]] ; then
for try in 1 2 3 ; do
echo "${content}" | base64 -d | \
if [[ "${prompting}" -eq 0 ]] ; then
ccrypt -d -E MOSH_MALLET_CCRYPT_KEY ; else
ccrypt -d ; fi && break
[[ "${prompting}" -eq 0 ]] && break ; done
else echo "discarded response" >&2 ; fi
else echo "no response" >&2 ; fi
else echo "request failed" >&2 ; fi
else echo "no target specified" >&2 ; fi
