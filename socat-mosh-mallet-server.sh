#!/bin/bash
MALLET_PORT=61000
[[ "${1}" =~ ^[1-9][0-9]{3,4}$ ]] && [[ "${1}" -ge 02000 && "${1}" -le 0xffff ]] && MALLET_PORT="${1}"
( dirname_re="(^.*/)[^/]+$" ; [[ "${BASH_SOURCE[0]}" =~ $dirname_re ]] && cd "${BASH_REMATCH[1]}"
socat -d -d -t 5 udp6-recvfrom:"${MALLET_PORT}",ipv6only=0,null-eof,fork exec:./mosh-mallet-server.sh,setsid )
