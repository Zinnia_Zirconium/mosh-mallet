#!/bin/bash
MOSH_PORTS="60001:60999 61000"
[[ ${#MOSH_MALLET_HMAC_KEY} -eq 0 ]] && echo \
"environment variable MOSH_MALLET_HMAC_KEY not set" >&2
[[ ${#MOSH_MALLET_CCRYPT_KEY} -eq 0 ]] && echo \
"environment variable MOSH_MALLET_CCRYPT_KEY not set" >&2
if [[ ${#MOSH_MALLET_HMAC_KEY} -ne 0 ]] ; then
if [[ ${#MOSH_MALLET_CCRYPT_KEY} -ne 0 ]] ; then
now="$(date -u '+%s')"
seconds_re="^[1-9][0-9]{9}$"
if [[ "${now}" =~ ${seconds_re} ]] ; then
request_stamp="$( php -r '
$request = stream_get_contents(STDIN, 42);
if (false !== $request) {
if (42 === strlen($request)) {
$request_hash = substr($request, 0, 32);
$request_time = substr($request, 32);
$key = getenv("MOSH_MALLET_HMAC_KEY");
if (false !== $key) {
$hash = hash_hmac("sha256", $request_time, $key, true);
if (false !== $hash) {
if (hash_equals($hash, $request_hash)) {
$seconds_re = "/^[1-9][0-9]{9}$/";
if (preg_match($seconds_re, $request_time)) {
echo $request_time; } } } } } }' )"
if [[ "${request_stamp}" =~ ${seconds_re} ]] ; then
elaps="$((${now}-${request_stamp}))"
absdd_re="^-?([0-9]{1,2})$"
if [[ "${elaps}" =~ ${absdd_re} ]] ; then
elaps="${BASH_REMATCH[1]}"
if [[ "${elaps}" -lt 60 ]] ; then
killall -q -w -o 150s mosh-server
for port in ${MOSH_PORTS} ; do
env -i PATH="${PATH}" LANG="${LANG}" mosh-server new -i :: -p "${port}"
done | ccrypt -E MOSH_MALLET_CCRYPT_KEY | \
php -r '
$response = stream_get_contents(STDIN);
if (false !== $response) {
if (32 < strlen($response)) {
$key = getenv("MOSH_MALLET_HMAC_KEY");
if (false !== $key) {
$hash = hash_hmac("sha256", $response, $key, true);
if (false !== $hash) {
echo $hash . $response;
sleep(1); } } } }'
fi ; fi ; fi ; fi ; fi ; fi
