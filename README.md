# Mosh Mallet

Single packet authorization UDP port knocking for Mosh without SSH.

Mosh Mallet whacks mosh-server and grabs MOSH_KEY for use by mosh-client.

It sends one UDP packet. It receives one UDP packet. It removes the need for SSH to start Mosh.

Usage on server:
> MOSH_MALLET_HMAC_KEY="First Secret Passphrase" MOSH_MALLET_CCRYPT_KEY="Second Secret Passphrase" bash socat-mosh-mallet-server.sh 61000

Usage on client:
> bash mosh-mallet.sh 192.0.2.192 61000

> bash mosh-mallet.sh 2001:db8::2021 61000

The ultimate design goal is not to install a client.

So **mosh-mallet-lazy.sh** is a one line script which is meant to be memorized. 

Mosh Mallet uses commonly available command line tools:

* php (for hash_hmac)
* ccrypt (for encryption)
* netcat (for client)
* socat (for server)

Since these particular tools are available in Cygwin and Termux.
